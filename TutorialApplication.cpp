/*
-----------------------------------------------------------------------------
Filename:    TutorialApplication.cpp
-----------------------------------------------------------------------------

This source file is part of the
   ___                 __    __ _ _    _
  /___\__ _ _ __ ___  / / /\ \ (_) | _(_)
 //  // _` | '__/ _ \ \ \/  \/ / | |/ / |
/ \_// (_| | | |  __/  \  /\  /| |   <| |
\___/ \__, |_|  \___|   \/  \/ |_|_|\_\_|
      |___/
Tutorial Framework (for Ogre 1.9)
http://www.ogre3d.org/wiki/
-----------------------------------------------------------------------------
*/

#include "TutorialApplication.h"

//---------------------------------------------------------------------------
TutorialApplication::TutorialApplication(void)
{
}
//---------------------------------------------------------------------------
TutorialApplication::~TutorialApplication(void)
{

}
//---------------------------------------------------------------------------
void TutorialApplication::createScene(void)
{
	// Add light which illuminates all objects in the scene regardless of direction
	mSceneMgr->setAmbientLight(Ogre::ColourValue(0.2f, 0.2f, 0.2f));
 
	// Create a directional light that gives shadows
	Ogre::Light* light = mSceneMgr->createLight("MainLight");
	
	// Set the position of the directional light
	light->setPosition(20, 80, 50);
 
	// Position the camera
	mCamera->setPosition(60, 200, 70);

	// Look at the position with the camera
	mCamera->lookAt(0,0,0);
 
	// Create an instance of a computer graphic model entity
	Ogre::Entity* ent;

	// Loop to create ogre heads in the scene
	for (int i = 0; i < 6; i++)
	{
		// Create a new node object in the scene for the ogre head entity
		Ogre::SceneNode* headNode = mSceneMgr->getRootSceneNode()->createChildSceneNode();

		// Create an instance of a computer graphic model ogre head entity
		ent = mSceneMgr->createEntity("head" + Ogre::StringConverter::toString(i), "ogrehead.mesh");

		// Add the ogre head entity to the node
		headNode->attachObject(ent);
 
		// Calculate the angle to rotate the orientation of the ogre head
		Ogre::Radian angle(i + Ogre::Math::TWO_PI / 6);

		// Set the position of the ogre head node in the scene
		headNode->setPosition(75 * Ogre::Math::Cos(angle), 0, 75 * Ogre::Math::Sin(angle));
	}
 
	// Call the function to create and turn on the projector
	createProjector();

	// Loop to create the decal and decal filter materials
	for(unsigned int i = 0; i < ent->getNumSubEntities(); i++)
	{
		// Get the individual ogre head materials and call function to make decal and decal filter materials
		makeMaterialReceiveDecal(ent->getSubEntity(i)->getMaterialName());
	}
}
//---------------------------------------------------------------------------
void TutorialApplication::createFrameListener(void)
{
	// Call the BaseApplication virtual function because we still want its functionality
	BaseApplication::createFrameListener();
}
//---------------------------------------------------------------------------
void TutorialApplication::destroyScene(void)
{
}
//---------------------------------------------------------------------------
bool TutorialApplication::frameRenderingQueued(const Ogre::FrameEvent& fe)
{
	// Call the BaseApplication virtual function because we still want its functionality
	bool ret = BaseApplication::frameRenderingQueued(fe);

	// Rotate the projector within the scene
	mProjectorNode->rotate(Ogre::Vector3::UNIT_Y, Ogre::Degree(fe.timeSinceLastFrame * 10));

	return ret;
}
//---------------------------------------------------------------------------
bool TutorialApplication::keyPressed(const OIS::KeyEvent& ke)
{
	// Call the BaseApplication virtual function because we still want its functionality
	BaseApplication::keyPressed(ke);

	return true;
}
//---------------------------------------------------------------------------
bool TutorialApplication::keyReleased(const OIS::KeyEvent& ke)
{
	// Call the BaseApplication virtual function because we still want its functionality
	BaseApplication::keyReleased(ke);

	return true;
}
//---------------------------------------------------------------------------
bool TutorialApplication::mouseMoved(const OIS::MouseEvent& me)
{
	// Call the BaseApplication virtual function because we still want its functionality
	BaseApplication::mouseMoved(me);

	return true;
}
//---------------------------------------------------------------------------
bool TutorialApplication::mousePressed(const OIS::MouseEvent& me, OIS::MouseButtonID id)
{
	// Call the BaseApplication virtual function because we still want its functionality
	BaseApplication::mousePressed(me, id);

	return true;
}
//---------------------------------------------------------------------------
bool TutorialApplication::mouseReleased(const OIS::MouseEvent& me, OIS::MouseButtonID id)
{
	// Call the BaseApplication virtual function because we still want its functionality
	BaseApplication::mouseReleased(me, id);

	return true;
}
//---------------------------------------------------------------------------
void TutorialApplication::createProjector()
{
	// Create the first frustum for the decal projector
	mDecalFrustum = new Ogre::Frustum();

	// Add the decal projector node to the scene
	mProjectorNode = mSceneMgr->getRootSceneNode()->createChildSceneNode("DecalProjectorNode");

	// Attach the decal projector node to the frustum
	mProjectorNode->attachObject(mDecalFrustum);

	// Position the decal projector node
	mProjectorNode->setPosition(0, 5, 0);
 
	// Create the second frustum for the decal filter projector
	mFilterFrustum = new Ogre::Frustum();

	// Set the type of frustum projector (orthographic or perspective)
	mFilterFrustum->setProjectionType(Ogre::PT_ORTHOGRAPHIC);

	// Create a new node object and add it to the decal projector node
	Ogre::SceneNode* filterNode = mProjectorNode->createChildSceneNode("DecalFilterNode");

	// Attach the decal filter projector node to the frustum
	filterNode->attachObject(mFilterFrustum);

	// Rotate the decal filter node
	filterNode->setOrientation(Ogre::Quaternion(Ogre::Degree(90), Ogre::Vector3::UNIT_Y));
}
//---------------------------------------------------------------------------
void TutorialApplication::makeMaterialReceiveDecal(const Ogre::String& matName)
{
	// Get the ogre head material
	Ogre::MaterialPtr mat = (Ogre::MaterialPtr)Ogre::MaterialManager::getSingleton().getByName(matName);

	// Create a new pass object for the ogre head materials technique
	Ogre::Pass* pass = mat->getTechnique(0)->createPass();
 
	// Set the pass object scene blending so that we don't get smears at the edge of our texture
	pass->setSceneBlending(Ogre::SBT_TRANSPARENT_ALPHA);

	// Set the pass object depth bias for the ogre head material so the decal will always be on top
	pass->setDepthBias(1);

	// Set dynamic lighting off so the material will be constant
	pass->setLightingEnabled(false);
 
	// Create a new texture unit state for the pass object using the decal
	Ogre::TextureUnitState* texState = pass->createTextureUnitState("decal.png");

	// Generate texture based on the frustum that projects the texture onto the surface
	texState->setProjectiveTexturing(true, mDecalFrustum);

	// Don't repeat the decal on the ogre head object
	texState->setTextureAddressingMode(Ogre::TextureUnitState::TAM_CLAMP);

	// Prevent transparent edges of the decal from being blurred into the ogre head material
	texState->setTextureFiltering(Ogre::FO_POINT, Ogre::FO_LINEAR, Ogre::FO_NONE);
 
	// Create a new texture unit state for the pass object using the decal filter
	texState = pass->createTextureUnitState("decal_filter.png");

	// Generate texture based on the frustum that projects the texture onto the surface
	texState->setProjectiveTexturing(true, mFilterFrustum);

	// Don't repeat the decal on the ogre head object
	texState->setTextureAddressingMode(Ogre::TextureUnitState::TAM_CLAMP);

	// Don't do texture filtering for the decal filter
	texState->setTextureFiltering(Ogre::TFO_NONE);
}
//---------------------------------------------------------------------------

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
#define WIN32_LEAN_AND_MEAN
#include "windows.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
	INT WINAPI WinMain(HINSTANCE hInst, HINSTANCE, LPSTR strCmdLine, INT)
#else
	int main(int argc, char *argv[])
#endif
	{
		// Create application object
		TutorialApplication app;

		try {
			app.go();
		} catch(Ogre::Exception& e)  {
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
			MessageBox(NULL, e.getFullDescription().c_str(), "An exception has occurred!", MB_OK | MB_ICONERROR | MB_TASKMODAL);
#else
			std::cerr << "An exception has occurred: " <<
			e.getFullDescription().c_str() << std::endl;
#endif
		}

		return 0;
	}

#ifdef __cplusplus
}
#endif

//---------------------------------------------------------------------------