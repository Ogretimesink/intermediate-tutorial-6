/*
-----------------------------------------------------------------------------
Filename:    TutorialApplication.h
-----------------------------------------------------------------------------

This source file is part of the
   ___                 __    __ _ _    _
  /___\__ _ _ __ ___  / / /\ \ (_) | _(_)
 //  // _` | '__/ _ \ \ \/  \/ / | |/ / |
/ \_// (_| | | |  __/  \  /\  /| |   <| |
\___/ \__, |_|  \___|   \/  \/ |_|_|\_\_|
      |___/
Tutorial Framework (for Ogre 1.9)
http://www.ogre3d.org/wiki/
-----------------------------------------------------------------------------
*/

#ifndef __TutorialApplication_h_
#define __TutorialApplication_h_

#include <Terrain/OgreTerrain.h>
#include <Terrain/OgreTerrainGroup.h>

#include "BaseApplication.h"
#include "SelectionBox.h"

//---------------------------------------------------------------------------

class TutorialApplication : public BaseApplication
{
public:
	// C++ application constructor
	TutorialApplication(void);

	// C++ application destructor
	virtual ~TutorialApplication(void);

private:
	// Function to fill the scene with graphic objects
	virtual void createScene(void);

	// Function to initialize screen overlay objects
	virtual void createFrameListener(void);

	// Function to delete scene objects
	virtual void destroyScene();

	// Function to update the scene every frame
	virtual bool frameRenderingQueued(const Ogre::FrameEvent& fe);

	// Function to process keyboard input
	virtual bool keyPressed(const OIS::KeyEvent& ke);

	// Function to process keyboard input
	virtual bool keyReleased(const OIS::KeyEvent& ke);
 
	// Function to process mouse input
	virtual bool mouseMoved(const OIS::MouseEvent& me);

	// Function to process mouse input
	virtual bool mousePressed(const OIS::MouseEvent& me, OIS::MouseButtonID id);

	// Function to process mouse input
	virtual bool mouseReleased(const OIS::MouseEvent& me, OIS::MouseButtonID id);

	// Function to create and turn on the decal and decal filter projectors
	virtual void createProjector();

	// Function to make decal and decal filter materials
	virtual void makeMaterialReceiveDecal(const Ogre::String& matName);
 
	// Scene node object for the decal and filter projectors
	Ogre::SceneNode* mProjectorNode;

	// Frustum for projecting the decal
	Ogre::Frustum* mDecalFrustum;

	// Frustum for projecting the decal filter
	Ogre::Frustum* mFilterFrustum;
};

//---------------------------------------------------------------------------

#endif // #ifndef __TutorialApplication_h_

//---------------------------------------------------------------------------
